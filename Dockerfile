FROM openjdk:8
COPY target/*.jar /opt/app.jar
CMD java -jar /opt/app.jar
EXPOSE 8080
